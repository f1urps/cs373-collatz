#!/usr/bin/env python3

# ----------
# Collatz.py
# ----------

# pylint: disable = bad-whitespace
# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# -------
# imports
# -------

from typing import IO, List, Dict
import math

# ------
# Caches
# ------

# Cache of cycle length for integers.
# Maps an integer n to the cycle length of n.
cycle_cache: Dict[int, int] = dict()

# Cache of max cycle length over ranges.
# Maps the end index of a range to the max cycle length.
# Size of the ranges are tile_size.
#     e.g. The range [101, 200] would be cached under range_cache[200]
# Initially, the cache only tiles up to a certain limit.
# If inputs surpassing that limit are seen, more tiles will be computed.
range_cache: Dict[int, int] = dict()

# ----------------------
# Range Cache Parameters
# ----------------------

tile_size = 100  # Size of cached tiles
initial_limit = 10000  # Inititialize to cover this range

# ------------
# collatz_read
# ------------


def collatz_read(s: str) -> List[int]:
    """
    read two ints
    s a string
    return a list of two ints, representing the beginning and end of a range, [i, j]
    """
    a = s.split()
    return [int(a[0]), int(a[1])]


# -----------------
# collatz_rc_resize
# -----------------


def collatz_rc_resize(new_size: int) -> None:
    """
    new_size the new minimum size of the range cache
    Resizes the range cache.
    Computes new tiles to cover the minimum that includes new_size.
        e.g. After calling collatz_rc_resize(302810), the highest range
        in the cache will be [302801, 302900].
    """
    current_size = len(range_cache) * tile_size

    assert current_size >= 0
    assert current_size % tile_size == 0
    assert new_size > 0

    if new_size > current_size:
        # Round up new_size to a multiple of tile_size
        new_size = math.ceil(float(new_size) / tile_size) * tile_size
        # Append new ranges to the end of the cache, computing max cl for each
        for j in range(current_size + tile_size, new_size + tile_size, tile_size):
            i = j - tile_size + 1
            max_cl = collatz_max_cycle_length(i, j)
            range_cache[j] = max_cl


# -------------
# collatz_cycle
# -------------


def collatz_cycle(n: int) -> int:
    """
    n the number to compute cycle length of
    return the cycle length of n
    """
    assert n > 0

    cl = 1  # Cycle length
    cycle = [n]  # The cycle itself, so intermediate steps can be cached
    m = n  # The current number in the cycle

    while m > 1:
        # Check the cache at each step.
        if m in cycle_cache:
            cl += cycle_cache[m] - 1
            break

        if m % 2 == 0:
            m = m >> 1
            cl += 1
            cycle.append(m)

        else:
            # In odd case, do two steps at once,
            # because 3n+1 will be even for any odd n
            m = m + (m >> 1) + 1  # (3n + 1) / 2
            cl += 2
            cycle.append(m << 1)
            cycle.append(m)

    # Use cycle list to add all steps in the cycle to cache.
    i = 0
    for intermediate_step in cycle:
        if intermediate_step not in cycle_cache:
            cycle_cache[intermediate_step] = cl - i
        i += 1

    assert cl > 0
    return cl


# ------------------------
# collatz_max_cycle_length
# ------------------------


def collatz_max_cycle_length(i: int, j: int) -> int:
    """
    i the beginning of the range, inclusive
    j the end of the range, inclusive
    returns the max cycle length of the range [i, j]
    """
    assert i > 0
    assert j >= i

    # If i is less than half of j, the range can
    # be reduced by that amount
    m = (j >> 1) + 1
    if i < m:
        i = m

    max_cl = 1
    for n in range(i, j + 1):
        cl = collatz_cycle(n)
        max_cl = max(max_cl, cl)

    assert max_cl > 0
    return max_cl


# ------------
# collatz_eval
# ------------


def collatz_eval(i: int, j: int) -> int:
    """
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    return the max cycle length of the range [i, j]
    """
    # Swap j and i if i > j
    if i > j:
        i, j = (j, i)

    assert i > 0
    assert j > 0
    assert i <= j

    max_cl = 1

    # Resize the range cache if necessary to make sure the highest
    # range includes j
    collatz_rc_resize(j)

    # Find all the cached tiles within [i, j] and take the max.
    # Find the end indices of the first and last tile within [i, j]
    range_cache_first = int(math.ceil(float(i - 1) / tile_size) + 1) * tile_size
    range_cache_last = j // tile_size * tile_size
    for k in range(range_cache_first, range_cache_last + tile_size, tile_size):
        tile_max_cl = range_cache[k]
        max_cl = max(max_cl, tile_max_cl)

    # Find the max cycle lengths of the two "endcaps", AKA the ranges on
    # either end of [i, j] that consist of a partial tile.
    #
    #  i   |<- 100 ->|<- 100 ->|<- 100 ->|   j
    #  |cap|  tile   |  tile   |  tile   |cap|

    # First endcap: from i to just before the first tile
    first_endcap_end = range_cache_first - tile_size
    if i <= first_endcap_end <= j:
        first_endcap_max_cl = collatz_max_cycle_length(i, first_endcap_end)
        max_cl = max(max_cl, first_endcap_max_cl)

    # Last endcap: from just after the last tile to j
    last_endcap_start = max(range_cache_last + 1, i)
    if last_endcap_start <= j:
        last_endcap_max_cl = collatz_max_cycle_length(last_endcap_start, j)
        max_cl = max(max_cl, last_endcap_max_cl)

    assert max_cl > 0
    return max_cl


# -------------
# collatz_print
# -------------


def collatz_print(w: IO[str], i: int, j: int, v: int) -> None:
    """
    print three ints
    w a writer
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    v the max cycle length
    """
    w.write(str(i) + " " + str(j) + " " + str(v) + "\n")


# -------------
# collatz_solve
# -------------


def collatz_solve(r: IO[str], w: IO[str]) -> None:
    """
    r a reader
    w a writer
    """
    # initialize range cache
    collatz_rc_resize(initial_limit)

    for s in r:
        i, j = collatz_read(s)
        v = collatz_eval(i, j)
        collatz_print(w, i, j, v)
