#!/usr/bin/env python3

# --------------
# TestCollatz.py
# --------------

# pylint: disable = bad-whitespace
# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve


class TestCollatz(TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "5 150\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 5)
        self.assertEqual(j, 150)

    def test_read_2(self):
        s = "10000 100\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 10000)
        self.assertEqual(j, 100)

    def test_read_3(self):
        s = "1234 1234\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 1234)
        self.assertEqual(j, 1234)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(30, 40)
        self.assertEqual(v, 107)

    def test_eval_2(self):
        v = collatz_eval(100, 1000)
        self.assertEqual(v, 179)

    def test_eval_3(self):
        v = collatz_eval(10, 1)
        self.assertEqual(v, 20)

    def test_eval_4(self):
        v = collatz_eval(50, 50)
        self.assertEqual(v, 25)

    def test_eval_5(self):
        v = collatz_eval(101, 250)
        self.assertEqual(v, 128)

    def test_eval_6(self):
        v = collatz_eval(1050, 2050)
        self.assertEqual(v, 182)

    def test_eval_7(self):
        v = collatz_eval(14, 13)
        self.assertEqual(v, 18)

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 50, 100, 1)
        self.assertEqual(w.getvalue(), "50 100 1\n")

    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 1000000, 1000000, 1000000)
        self.assertEqual(w.getvalue(), "1000000 1000000 1000000\n")

    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 10, 10, 99)
        self.assertEqual(w.getvalue(), "10 10 99\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("7935 8210\n69 4032\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(w.getvalue(), "7935 8210 252\n69 4032 238\n")

    def test_solve_2(self):
        r = StringIO("1115 2191\n2564 2777\n6776 9666\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(w.getvalue(), "1115 2191 182\n2564 2777 191\n6776 9666 260\n")

    def test_solve_3(self):
        r = StringIO("")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(w.getvalue(), "")

    def test_solve_4(self):
        r = StringIO("6248 92\n8452 9351\n9993 9227\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(w.getvalue(), "6248 92 262\n8452 9351 260\n9993 9227 260\n")


# ----
# main
# ----

if __name__ == "__main__":  # pragma: no cover
    main()
